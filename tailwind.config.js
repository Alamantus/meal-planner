module.exports = {
  mode: 'jit',
  content: [
    './public/**/*.html',
    './src/**/*.js',
  ],
};
