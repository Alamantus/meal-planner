import { State } from './State';
import { Emitter } from './Emitter';
import { Router } from './Router';

export default (app) => {
  app.use(State.initialize);
  app.use(Emitter.initialize);
  Router.initialize(app);
  app.mount('body');
}