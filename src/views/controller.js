export class ViewController {
  constructor(state, viewName, defaultState = {}) {
    // Store the global app state so it's accessible but out of the way.
    this.appState = state;
    if (this.appState.view !== viewName) {
      this.appState.view = viewName;
    }

    // Give this view its own state within the appState.
    if (!this.appState.viewStates.hasOwnProperty(viewName)) {
      this.appState.viewStates[viewName] = defaultState;
    }
    this.state = this.appState.viewStates[viewName];
  }
}
