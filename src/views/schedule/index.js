import html from 'choo/html';
import { ScheduleController } from './controller';

export const scheduleView = (state, emit) => {
  const controller = new ScheduleController(state);
  emit('DOMTitleChange', controller.appState.view);

  return html`<section>
</section>`;
};