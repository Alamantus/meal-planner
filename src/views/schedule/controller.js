import { getWeek, setWeek } from "date-fns";
import { ViewController } from "../controller";

export class ScheduleController extends ViewController {
  constructor(state) {
    super(state, 'Schedule', {
      week: getWeek(new Date()),
    });

  }
}