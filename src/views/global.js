import html from 'choo/html';
import { SITE_NAME } from '../constants';
import { navLink } from '../components/navLink';

export const globalView = (state, emit, view) => {
  return html`<body>
  <header class="p-4">
    <nav>
      <ul class="flex">
        <li class="flex-1">
          <h1><a href="/">${SITE_NAME}</a></h1>
        </li>
        <li class="flex-initial">
          ${navLink(state, '/', 'Schedule')}
        </li>
        <li class="flex-initial">
          ${navLink(state, '#recipes', 'Recipes')}
        </li>
      </ul>
    </nav>
  </header>
  <main class="container mx-auto">
    ${view(state, emit)}
  </main>
  <footer class="p-4">
    <p>${'\u00A9'} ${(new Date()).getFullYear()} ${SITE_NAME}</p>
  </footer>
</body>`;
};