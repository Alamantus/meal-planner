import { globalView } from "./views/global";
import { scheduleView } from "./views/schedule";

export class Router {
  static initialize(app) {
    app.route('/', (state, emit) => globalView(state, emit, scheduleView));
    app.route('/recipes', (state, emit) => globalView(state, emit, scheduleView));
  }
}