export class Emitter {
  static initialize(state, emitter) {
    emitter.on('DOMContentLoaded', () => {
      emitter.emit('render');
    });
    emitter.on('view', (view, params = {}) => {
      state.view = view;
      state.viewState = params;
      emitter.emit('render');
    });
  }
}