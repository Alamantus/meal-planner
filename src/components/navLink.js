import html from 'choo/html';

export const navLink = (state, url, label) => {
  const active = state.route === url || state.route === url.substr(1);
  return html`<a href="${url}" class="p-4 hover:bg-green-600 hover:text-white${active ? ' bg-green-800 text-white' : ''}">
  ${label}
</a>`;
}
