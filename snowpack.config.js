module.exports = {
  exclude: [
    '**/.git/**/*', // I *can't believe* this is required!
  ],
  plugins: [
    '@snowpack/plugin-postcss',
    // [ preload-render ],
  ],
  devOptions: {
    tailwindConfig: './tailwind.config.js',
  },
};
