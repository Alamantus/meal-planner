import choo from 'choo';
import init from './src/init';

const app = choo({ hash: true });

if (import.meta.env.MODE !== 'production') {
  import('choo-devtools').then(exports => {
    app.use(exports.default());
    init(app);
  });
} else {
  init(app);
}
